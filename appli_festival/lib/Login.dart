// ignore_for_file: file_names
import 'dart:collection';

import 'package:appli_festival/Objects/ResponseApi.dart';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _login_PageState createState() => _login_PageState();
}

class _login_PageState extends State<LoginPage> {
  final EmailController = TextEditingController();
  final PasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {

    final title = "Login";

    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(title),
      ),
      body: Center(

        child: Column(

          mainAxisAlignment: MainAxisAlignment.center,
          children:  <Widget>[
            TextField(
              controller: EmailController,
              obscureText: false,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Email',
              ),
            ),
            const SizedBox(height: 30),
            TextField(
              controller: PasswordController ,
              obscureText: true,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Mot de passe',
              ),
            ),

            const SizedBox(height: 30),
            ElevatedButton(
              onPressed: ()=>login(EmailController.text,PasswordController.text)  ,
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                backgroundColor: MaterialStateProperty.all<Color>(Colors.blueAccent),
              ),
              child: const Text('Connexion'),
            ),
            const SizedBox(height: 10),
            ElevatedButton(
              onPressed:  () =>Navigator.of(context).pushNamed('/register')  ,
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                backgroundColor: MaterialStateProperty.all<Color>(Colors.blueAccent),
              ),
              child: const Text('Inscription'),
            ),
          ],
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

 void  login(String Email,String Pass) async {

    var response = await http.post(
      Uri.parse('http://10.0.2.2:90/userconnection/Login'),

      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'login': Email,
        'password': Pass,
      }),
    );

    ResponseAPi reponse = new ResponseAPi();

    Map<String, dynamic> userdata = new Map<String, dynamic>.from(json.decode(response.body.toString()));


    reponse.statusCode = userdata.values.elementAt(0);
    reponse.message = userdata.values.elementAt(1);

    if(reponse.statusCode == 200 && Email == "paul.geslin@epsi.fr"){
      ScaffoldMessenger.of(context).showSnackBar(new SnackBar(content: Text("Administrateur")));
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString('Token', reponse.message);
      Navigator.of(context).pushNamed('/festivalAdmin');

    }else if(response.statusCode == 200){
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString('Token', reponse.message);
      Navigator.of(context).pushNamed('/festival');
    }else{
      ScaffoldMessenger.of(context).showSnackBar(new SnackBar(content: Text(reponse.message)));
      EmailController.clear();
      PasswordController.clear();
    }
    }
  }

