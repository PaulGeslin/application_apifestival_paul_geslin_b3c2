// ignore_for_file: file_names


import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import 'Objects/ArtisteClass.dart';

class FestivalArtistes extends StatefulWidget {
  const FestivalArtistes({Key? key}) : super(key: key);

  @override
  _ArtisteState createState() => _ArtisteState();
}

class _ArtisteState extends State<FestivalArtistes> {
  List<ArtisteClass> artistes = <ArtisteClass>[];
   var id;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    GetArtiste();
  }

  @override
  Widget build(BuildContext context) {
    id = ModalRoute.of(context)!.settings.arguments;

    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text("Programmation"),
      ),
      body: Column(
        children: [
          Expanded(
            child: buildListView(),
          ),
        ],
      ),
    );
  }
  ListView buildListView() {
    return ListView.separated(
        itemCount: artistes.length,
        separatorBuilder: (context, index) => Divider(),
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(
              artistes[index].Nom,
                style: const TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.normal,
                  fontSize: 15.0)
            ),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                IconButton(
                  icon: const Icon(
                    Icons.more,
                    size: 20.0
                  )
                  , onPressed: () {
                  Navigator.pushNamed(
                      context,
                      '/detailArtiste',
                      arguments: artistes[index].Id
                  );
                },
                ),
              ],
            ),

          );

        }
    );
  }

  GetArtiste() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.getString("Token")! ;

    var response = await http.get(
      Uri.parse('http://10.0.2.2:90/Festival/GetArtistes/'+ id.toString()),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    Map<String, dynamic> Festivaldata = Map<String, dynamic>.from(json.decode(response.body.toString()
    ));


    var Result = Festivaldata.values.elementAt(4);

    for(int i=0;i<Result.length;i++){

      var Artiste = ArtisteClass();

      Artiste.Id =  Result[i].values.elementAt(0);
      Artiste.IdFestival =  Result[i].values.elementAt(1);
      Artiste.Nom =  Result[i].values.elementAt(2);
      Artiste.Description =  Result[i].values.elementAt(3);

      artistes.add(Artiste);
    }


    setState(() {

    });
  }
}
