// ignore_for_file: file_names

import 'dart:collection';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:appli_festival/Objects/FestivalClass.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import 'Objects/ResponseApi.dart';


class FestivalAdd extends StatefulWidget {

  const FestivalAdd({Key? key}) : super(key: key);


  @override
  _FestivalAddState createState() => _FestivalAddState();

}

class _FestivalAddState extends State<FestivalAdd> {
  DateTime DateDebut = DateTime.now();

  final NomController = TextEditingController();
  final RueController = TextEditingController();
  final VilleController = TextEditingController();
  final DepartementController = TextEditingController();
  final PaysController = TextEditingController();

  List<FestivalClass> Festivals =  <FestivalClass>[];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context)  {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text("Ajout festival"),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children:  <Widget>[
            TextField(
              controller: NomController,
              obscureText: false,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Nom',
              ),
            ),
            const SizedBox(height: 5),
            TextField(
              controller: RueController,
              obscureText: false,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Rue',
              ),
            ),
            const SizedBox(height: 5),
            TextField(
              controller: VilleController,
              obscureText: false,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Ville',
              ),
            ),
            const SizedBox(height: 5),
            TextField(
              controller: DepartementController,
              obscureText: false,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Département',
              ),
            ),
            const SizedBox(height: 5),
            ElevatedButton(
              onPressed: () => _selectDate(context),
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                backgroundColor: MaterialStateProperty.all<Color>(Colors.blueAccent),
              ),
              child: const Text('Date de début'),
            ),
            const SizedBox(height: 5),
            ElevatedButton(
              onPressed: () => AddFestival()  ,
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                backgroundColor: MaterialStateProperty.all<Color>(Colors.blueAccent),
              ),
              child: const Text('Valider '),
            ),
          ],
        ),
      ),
    );
  }

  _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateDebut,
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != DateDebut) {
      setState(() {
        DateDebut = picked;
      });
    }
  }

  AddFestival() async {
    var prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("Token")! ;

    if(NomController.text == "" || RueController.text =="" || DepartementController.text ==""|| VilleController.text ==""){
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Veuillez saisir les informations")));
    }else{
      var response = await http.post(
        Uri.parse('http://10.0.2.2:90/Festival'),

        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': "Bearer " + token
        },
        body: jsonEncode(<String, String>{
          'nom': NomController.text,
          'rue': RueController.text,
          'departement': DepartementController.text,
          'pays': "France",
          'ville': VilleController.text,
          'dateDebut': DateTime.now().toString(),
        }),
      );

      ResponseAPi reponse = ResponseAPi();

      Map<String, dynamic> userdata = Map<String, dynamic>.from(json.decode(response.body.toString()));

      reponse.statusCode = userdata.values.elementAt(0);
      reponse.message = userdata.values.elementAt(1);


      if(reponse.statusCode == 200){
        Navigator.pushNamed(
          context,
          '/festivalAdmin'
        );
        setState(() {

        });
      }else{
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(reponse.message)));
      }

      setState(() {
      });
    }
  }
}
