import 'dart:convert';

import 'package:appli_festival/Admin/EditFestival.dart';
import 'package:appli_festival/Admin/FestivalAdmin.dart';
import 'package:appli_festival/Admin/ProgrammationAddAdmin.dart';
import 'package:appli_festival/ArtisteDetail.dart';
import 'package:appli_festival/FestivalAdd.dart';
import 'package:appli_festival/LocalisationFestival.dart';
import 'package:flutter/material.dart';
import 'package:appli_festival/Login.dart';
import 'package:appli_festival/Festival.dart';
import 'package:appli_festival/FestivalArtistes.dart';
import 'package:appli_festival/FestivalDetail.dart';

import 'Admin/FestivalArtistesAdmin.dart';
import 'FestivalDetail.dart';
import 'Register.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      routes: <String, WidgetBuilder>{
        '/Login' : (BuildContext context) => LoginPage(),
        '/register' : (BuildContext context) => RegisterPage(),
        '/festival' : (BuildContext context) => Festival(),
        '/festival/Artistes' : (BuildContext context) => FestivalArtistes(),
        '/festival/ArtistesAdmin' : (BuildContext context) => FestivalArtistesAdmin(),
        '/detailArtiste' : (BuildContext context) => ArtisteDetail(),
        '/DetailFestival' : (BuildContext context) => FestivalDetail(),
        '/LocalisationFestival' : (BuildContext context) => LocalisationFestival(),
        '/festivalAdmin' : (BuildContext context) => FestivalAdmin(),
        '/festivalAjout' : (BuildContext context) => FestivalAdd(),
        '/ArtistesAjout' : (BuildContext context) => ProgrammationAddAdmin(),
        '/editFestival' : (BuildContext context) => EditFestival(),
      },
      initialRoute: '/Login',
    );

  }
}



