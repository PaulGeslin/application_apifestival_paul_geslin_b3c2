// ignore_for_file: file_names
import 'dart:convert';

import 'package:appli_festival/Objects/FestivalClass.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

class FestivalDetail extends StatefulWidget {
  const FestivalDetail({Key? key}) : super(key: key);

  @override
  _FestivalDetailState createState() => _FestivalDetailState();
}

class _FestivalDetailState extends State<FestivalDetail> {
  FestivalClass festival = FestivalClass();
  final NomController = TextEditingController();
  final DateController = TextEditingController();

  var id;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    GetArtiste();
  }

  @override
  Widget build(BuildContext context) {
    id = ModalRoute.of(context)!.settings.arguments;

    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text("Détail"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextField(
            controller: NomController,
            readOnly: true,
            obscureText: false,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: "Nom du festival",
            ),
          ),
          const SizedBox(height: 30), TextField(
            controller: DateController,
            readOnly: true,
            obscureText: false,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: "Date du festival",
            ),
          ),
          const SizedBox(height: 30),
        ],
      ),
    );
  }

  GetArtiste() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.getString("Token")! ;

    var response = await http.get(
      Uri.parse('http://10.0.2.2:90/Festival/'+ id.toString()),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    Map<String, dynamic> Festivaldata = Map<String, dynamic>.from(json.decode(response.body.toString()
    ));

    var Result = Festivaldata.values.elementAt(4);

    festival.Id = Result.values.elementAt(0);
    NomController.text =  Result.values.elementAt(1);
    DateController.text =  DateFormat("dd-MM-yyyy").format(DateTime.parse(Result.values.elementAt(6).toString())).toString();


    setState(() {

    });
  }

}



