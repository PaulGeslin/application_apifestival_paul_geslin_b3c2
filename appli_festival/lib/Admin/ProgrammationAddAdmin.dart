// ignore_for_file: file_names

import 'dart:collection';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:appli_festival/Objects/FestivalClass.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../Objects/ResponseApi.dart';


class ProgrammationAddAdmin extends StatefulWidget {

  const ProgrammationAddAdmin({Key? key}) : super(key: key);


  @override
  _ProgrammationAddAdminState createState() => _ProgrammationAddAdminState();

}

class _ProgrammationAddAdminState extends State<ProgrammationAddAdmin> {
  DateTime DateDebut = DateTime.now();

  var id;

  final NomController = TextEditingController();
  final DescriptionController = TextEditingController();


  List<FestivalClass> Festivals =  <FestivalClass>[];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context)  {
    id = ModalRoute.of(context)!.settings.arguments;
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text("Programmer un artiste"),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const SizedBox(height: 10),
            TextField(
              controller: NomController,
              obscureText: false,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Nom',
              ),
            ),
            const SizedBox(height: 10),
            TextField(
              controller: DescriptionController,
              obscureText: false,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Description',
              ),
            ),
            const SizedBox(height: 10),
            ElevatedButton(
              onPressed: ()=> AddProgrammation()  ,
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                backgroundColor: MaterialStateProperty.all<Color>(Colors.blueAccent),
              ),
              child: const Text('Ajouter'),
            ),
          ],
        ),
      ),
    );
  }

  AddProgrammation() async {
    var prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("Token")! ;

    if(NomController.text == "" || DescriptionController.text ==""){
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Veuillez saisir les informations")));
    }else{

      var response = await http.post(
        Uri.parse('http://10.0.2.2:90/FestivalArtiste/FestvalAddArtiste/'+ id.toString()),

        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': "Bearer " + token
        },
        body: jsonEncode(<String, String>{
          'nom': NomController.text,
          'description': DescriptionController.text,
        }),
      );

      ResponseAPi reponse = ResponseAPi();

      Map<String, dynamic> userdata = Map<String, dynamic>.from(json.decode(response.body.toString()));

      reponse.statusCode = userdata.values.elementAt(0);
      reponse.message = userdata.values.elementAt(1);

      if(reponse.statusCode == 200){
        Navigator.of(context).pushNamed('/festivalAdmin');
        setState(() {

        });
      }else{
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(reponse.message)));
      }

      setState(() {
      });
    }
  }
}
