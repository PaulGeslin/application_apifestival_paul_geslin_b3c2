// ignore_for_file: file_names

import 'dart:collection';
import 'dart:convert';

import 'package:appli_festival/Objects/ResponseApi.dart';
import 'package:flutter/material.dart';
import 'package:appli_festival/Objects/FestivalClass.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;


class FestivalAdmin extends StatefulWidget {

  const FestivalAdmin({Key? key}) : super(key: key);

  @override
  _FestivalAdminState createState() => _FestivalAdminState();

}

class _FestivalAdminState extends State<FestivalAdmin> {

  final EmailController = TextEditingController();
  List<FestivalClass> Festivals =  <FestivalClass>[];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    GetFestivals();
  }

  @override
  Widget build(BuildContext context)  {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text("Festival"),
        actions: <Widget>[
          IconButton(
            icon: const Icon(
              Icons.add,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.of(context).pushNamed('/festivalAjout');
            },
          )
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: buildListView(),
          ),
        ],
      ),
    );
  }
  ListView buildListView() {
    return ListView.separated(
        itemCount: Festivals.length,
        separatorBuilder: (context, index) => Divider(),
        itemBuilder: (context, index) {
          return ListTile(
            onTap: () => Navigator.pushNamed(
              context,
              '/festival/ArtistesAdmin',
              arguments: Festivals[index].Id,
            ),
            title: Text(
                Festivals[index].Nom,
                style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                    fontSize: 15.0)
            ),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                IconButton(
                  icon: const Icon(
                      Icons.more,
                      size: 20.0
                  ),
                  onPressed: () {
                    Navigator.pushNamed(
                      context,
                      '/DetailFestival',
                      arguments: Festivals[index].Id,
                    );
                  },
                ),
                IconButton(
                  icon: const Icon(
                      Icons.location_on_outlined,
                      size: 20.0
                  ),
                  onPressed: () {
                    Navigator.pushNamed(
                      context,
                      '/LocalisationFestival',
                      arguments: Festivals[index].Id,
                    );
                  },
                ),IconButton(
                  icon: const Icon(
                      Icons.edit,
                      size: 20.0
                  ),
                  onPressed: () {
                    Navigator.pushNamed(
                      context,
                      '/editFestival',
                      arguments: Festivals[index].Id,
                    );
                  },
                ),IconButton(
                  icon: const Icon(
                      Icons.delete,
                      size: 20.0
                  ),
                  onPressed: () {
                    DeleteFestival(Festivals[index].Id);
                  },
                ),
              ],
            ),
          );
        }
    );
  }

  DeleteFestival(int id ) async {
    var prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("Token")! ;

    var response = await http.delete(
      Uri.parse('http://10.0.2.2:90/Festival/' + id.toString()),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': "Bearer " + token
      },
    );

    ResponseAPi reponseAPI = ResponseAPi();

    Map<String, dynamic> userdata = Map<String, dynamic>.from(json.decode(response.body.toString()));

    reponseAPI.statusCode = userdata.values.elementAt(0);
    reponseAPI.message = userdata.values.elementAt(1);

    if(reponseAPI.statusCode == 200){
      Navigator.of(context).pushNamed('/festivalAdmin');
    }else{
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(reponseAPI.message)));
    }


  }

  GetFestivals() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.getString("Token")! ;

    var response = await http.get(
      Uri.parse('http://10.0.2.2:90/Festival'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    Map<String, dynamic> Festivaldata = new Map<String, dynamic>.from(json.decode(response.body.toString()
    ));

    var Result = Festivaldata.values.elementAt(4);

    for(int i=0;i<Result.length;i++){
      var Festival = new FestivalClass();

      Festival.Id =  Result[i].values.elementAt(0);

      Festival.Nom =  Result[i].values.elementAt(1);
      Festival.Rue =  Result[i].values.elementAt(2);
      Festival.Departement =  Result[i].values.elementAt(3);
      Festival.Pays =  Result[i].values.elementAt(4);
      Festival.Ville =  Result[i].values.elementAt(5);

      Festivals.add(Festival);

    }

    setState(() {

    });
  }
}
