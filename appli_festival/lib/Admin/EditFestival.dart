// ignore_for_file: file_names
import 'dart:convert';

import 'package:appli_festival/Objects/FestivalClass.dart';
import 'package:appli_festival/Objects/ResponseApi.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class EditFestival extends StatefulWidget {
  const EditFestival({Key? key}) : super(key: key);



  @override
  _EditFestivalState createState() => _EditFestivalState();
}

class _EditFestivalState extends State<EditFestival> {

  DateTime DateDebut = DateTime.now();
  final NomController = TextEditingController();
  final RueController = TextEditingController();
  final VilleController = TextEditingController();
  final DepartementController = TextEditingController();
  final PaysController = TextEditingController();
  var id;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    GetFestival();
  }

  @override
  Widget build(BuildContext context) {
    id = ModalRoute.of(context)!.settings.arguments;
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text("Modification festival"),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children:  <Widget>[
            TextField(
              controller: NomController,
              obscureText: false,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Nom',
              ),
            ),
            const SizedBox(height: 5),
            TextField(
              controller: RueController,
              obscureText: false,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Rue',
              ),
            ),
            const SizedBox(height: 5),
            TextField(
              controller: VilleController,
              obscureText: false,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Ville',
              ),
            ),
            const SizedBox(height: 5),
            TextField(
              controller: DepartementController,
              obscureText: false,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Département',
              ),
            ),
            const SizedBox(height: 5),
            ElevatedButton(
              onPressed: () => _selectDate(context),
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                backgroundColor: MaterialStateProperty.all<Color>(Colors.blueAccent),
              ),
              child: const Text('Date de début'),
            ),
            const SizedBox(height: 5),
            ElevatedButton(
              onPressed: () => UpdateFestival()  ,
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                backgroundColor: MaterialStateProperty.all<Color>(Colors.blueAccent),
              ),
              child: const Text('Valider '),
            ),
          ],
        ),
      ),
    );

  }
  _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateDebut,
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != DateDebut) {
      setState(() {
        DateDebut = picked;
      });
    }
  }

  UpdateFestival() async {
    var prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("Token")! ;

    if(NomController.text == "" || RueController.text =="" || DepartementController.text ==""|| VilleController.text ==""){
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Veuillez saisir les informations")));
    }else{
      var response = await http.put(
        Uri.parse('http://10.0.2.2:90/Festival'),

        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': "Bearer " + token
        },
        body: jsonEncode(<String, String>{
          'id': id.toString(),
          'nom': NomController.text,
          'rue': RueController.text,
          'departement': DepartementController.text,
          'pays': "France",
          'ville': VilleController.text,
          'dateDebut': DateDebut.toString(),
        }),
      );

      ResponseAPi reponse = ResponseAPi();

      Map<String, dynamic> userdata = Map<String, dynamic>.from(json.decode(response.body.toString()));

      reponse.statusCode = userdata.values.elementAt(0);
      reponse.message = userdata.values.elementAt(1);

      if(reponse.statusCode == 200){
        Navigator.of(context).pushNamed('/festivalAdmin');
      }else{
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(reponse.message)));
      }

      setState(() {
      });
    }
  }

  GetFestival() async {
    var prefs = await SharedPreferences.getInstance();
    String token =  prefs.getString("Token")! ;

    var response = await http.get(
      Uri.parse('http://10.0.2.2:90/Festival/' + id.toString()),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': "Bearer " + token
      },
    );

    Map<String, dynamic> Festivaldata = new Map<String, dynamic>.from(json.decode(response.body.toString()
    ));

    var Result = Festivaldata.values.elementAt(4);

    var Festival = new FestivalClass();

    Festival.Id =  Result.values.elementAt(0);

    Festival.Nom =  Result.values.elementAt(1);
    Festival.Rue =  Result.values.elementAt(2);
    Festival.Departement =  Result.values.elementAt(3);
    Festival.Pays =  Result.values.elementAt(4);
    Festival.Ville =  Result.values.elementAt(5);

    NomController.text = Festival.Nom;
    RueController.text = Festival.Rue;
    VilleController.text = Festival.Ville;
    DepartementController.text = Festival.Departement;
    PaysController.text = Festival.Pays;

    setState(() {

    });
  }

}
