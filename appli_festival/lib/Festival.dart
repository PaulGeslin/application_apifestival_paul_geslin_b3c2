// ignore_for_file: file_names

import 'dart:collection';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:appli_festival/Objects/FestivalClass.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;


class Festival extends StatefulWidget {

  const Festival({Key? key}) : super(key: key);

  @override
  _FestivalState createState() => _FestivalState();

}

class _FestivalState extends State<Festival> {

  final EmailController = TextEditingController();
  List<FestivalClass> Festivals =  <FestivalClass>[];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    GetFestivals();
  }

  @override
  Widget build(BuildContext context)  {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text("Festival"),
      ),
      body: Column(
        children: [
          Expanded(
            child: buildListView(),
          ),
        ],
      ),
    );
  }
  ListView buildListView() {
    return ListView.separated(
        itemCount: Festivals.length,
        separatorBuilder: (context, index) => Divider(),
        itemBuilder: (context, index) {
          return ListTile(
            onTap: () => Navigator.pushNamed(
              context,
              '/festival/Artistes',
              arguments: Festivals[index].Id,
            ),
            title: Text(
                Festivals[index].Nom,
                style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                    fontSize: 15.0)
            ),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                IconButton(
                  icon: const Icon(
                      Icons.more,
                      size: 20.0
                  ),
                  onPressed: () {
                    Navigator.pushNamed(
                      context,
                      '/DetailFestival',
                      arguments: Festivals[index].Id,
                    );
                  },
                ),
                IconButton(
                  icon: const Icon(
                      Icons.location_on_outlined,
                      size: 20.0
                  ),
                  onPressed: () {
                    Navigator.pushNamed(
                      context,
                      '/LocalisationFestival',
                      arguments: Festivals[index].Id,
                    );
                  },
                ),
              ],
            ),

          );
        }
    );
  }

  GetFestivals() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.getString("Token")! ;

    var response = await http.get(
      Uri.parse('http://10.0.2.2:90/Festival'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    Map<String, dynamic> Festivaldata = new Map<String, dynamic>.from(json.decode(response.body.toString()
    ));

    var Result = Festivaldata.values.elementAt(4);

    for(int i=0;i<Result.length;i++){
      var Festival = new FestivalClass();

      Festival.Id =  Result[i].values.elementAt(0);

      Festival.Nom =  Result[i].values.elementAt(1);
      Festival.Rue =  Result[i].values.elementAt(2);
      Festival.Departement =  Result[i].values.elementAt(3);
      Festival.Pays =  Result[i].values.elementAt(4);
      Festival.Ville =  Result[i].values.elementAt(5);

      Festivals.add(Festival);

    }

    setState(() {

    });
  }
}
