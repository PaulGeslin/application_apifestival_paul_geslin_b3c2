// ignore_for_file: file_names
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'Objects/ArtisteClass.dart';

class ArtisteDetail extends StatefulWidget {
  const ArtisteDetail({Key? key}) : super(key: key);

  @override
  _ArtisteDetailState createState() => _ArtisteDetailState();
}

class _ArtisteDetailState extends State<ArtisteDetail> {
  ArtisteClass artiste = ArtisteClass();
  final NomController = TextEditingController();
  final DescriptionController = TextEditingController();

  var id;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    GetArtiste();
  }

  @override
  Widget build(BuildContext context) {
    id = ModalRoute.of(context)!.settings.arguments;



    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text("Détail"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextField(
            controller: NomController,
            readOnly: true,
            obscureText: false,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: "Nom de l'artiste",
            ),
          ),
          const SizedBox(height: 30), TextField(
            controller: DescriptionController,
            readOnly: true,
            obscureText: false,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: "Description",
            ),
          ),
          const SizedBox(height: 30),
        ],
      ),
    );
  }

  GetArtiste() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.getString("Token")! ;

    var response = await http.get(
      Uri.parse('http://10.0.2.2:90/Artiste/'+ id.toString()),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    Map<String, dynamic> Festivaldata = Map<String, dynamic>.from(json.decode(response.body.toString()
    ));

    var Result = Festivaldata.values.elementAt(4);

      artiste.Id = Result[0].values.elementAt(0);
      artiste.IdFestival =  Result[0].values.elementAt(1);
      NomController.text =  Result[0].values.elementAt(2);
      DescriptionController.text =  Result[0].values.elementAt(3);

    setState(() {

    });
  }

}



