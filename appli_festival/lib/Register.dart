// ignore_for_file: file_names
import 'dart:collection';

import 'package:appli_festival/Objects/ResponseApi.dart';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final EmailController = TextEditingController();
  final PasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {


    final title = "Inscription";

    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(title),
      ),
      body: Center(

        child: Column(

          mainAxisAlignment: MainAxisAlignment.center,
          children:  <Widget>[
            TextField(
              controller: EmailController,
              obscureText: false,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Email',
              ),
            ),
            const SizedBox(height: 30),
            TextField(
              controller: PasswordController ,
              obscureText: true,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Mot de passe',
              ),
            ),
            ElevatedButton(
              onPressed:  () =>Register(EmailController.text,PasswordController.text)  ,
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                backgroundColor: MaterialStateProperty.all<Color>(Colors.blueAccent),
              ),
              child: const Text('Inscription'),
            ),
          ],
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  void  Register(String Email,String Pass) async {

    var response = await http.post(
      Uri.parse('http://10.0.2.2:90/userconnection/Register'),

      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'role': 'client',
        'email': Email,
        'passwordHash': Pass,
        'userName': Email,
      }),
    );

    ResponseAPi reponse = new ResponseAPi();

    Map<String, dynamic> userdata = new Map<String, dynamic>.from(json.decode(response.body.toString()));


    reponse.statusCode = userdata.values.elementAt(0);
    reponse.message = userdata.values.elementAt(1);



    if( Pass == "" || Email == ""){
      ScaffoldMessenger.of(context).showSnackBar(new SnackBar(content: Text("Veuillez saisir les informations")));

    }else if(response.statusCode == 200){
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString('Token', reponse.message);
      Navigator.of(context).pushNamed('/festival');
    }else{
      ScaffoldMessenger.of(context).showSnackBar(new SnackBar(content: Text(reponse.message)));
      EmailController.clear();
      PasswordController.clear();
    }
  }
}

